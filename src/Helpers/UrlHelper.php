<?php
use Illuminate\Support\Str;

if (! function_exists('secure')) {
    function secure($url)
    {
        $replaced = str_replace('http://', '', $url);
        $replaced = Str::start($replaced, 'https://');

        return $replaced;
    }
}
