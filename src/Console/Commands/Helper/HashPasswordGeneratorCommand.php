<?php

namespace Ihero\Generator\Console\Commands\Helper;

use Illuminate\Support\Facades\Hash;
use Illuminate\Console\Command;

class HashPasswordGeneratorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ih.helper:hash-password {password : the password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generated a hash password';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line(
            Hash::make($this->argument('password'))
        );
    }
}
