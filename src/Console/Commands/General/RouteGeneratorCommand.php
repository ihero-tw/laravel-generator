<?php

namespace Ihero\Generator\Console\Commands\General;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;
use Ihero\Generator\Exceptions\RuntimeException;

class RouteGeneratorCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ih.route:write
                                {name : Write name of route.}
                                {scaffold : Specified scaffold.}
                                {--route= : Specified write route of laravel\'s file.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Write specified route to route\'s file.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Route';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        if (!$stub = config(sprintf('ih.stubs.%s.route', $this->argument('scaffold')))) {
            throw new RuntimeException('This scaffold route does not exist.');
        }

        return $stub;
    }

    /**
     * Execute the console command.
     *
     * @return bool|null
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        try {
            $route = $this->buildRoute($this->getNameInput(), $this->getStub());
            $routePath = $this->getRoutePath($this->option('route') ?? 'web');

            $this->writeRoute($routePath, $route);

            $this->info($this->type.' writed successfully.');
        } catch (RuntimeException $exception) {
            $this->error($exception->getMessage());
        }
    }

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @param  string  $stub
     * @return string
     */
    protected function buildRoute(string $name, string $stub): string
    {
        $stub = $this->files->get($stub);
        $replace = $this->buildReplacements($name);

        return $this
            ->replaceReplacements($stub, $replace);
    }

    /**
     * Get route file path.
     *
     * @param string $filename
     * @return string
     */
    public function getRoutePath(string $filename): string
    {
        $route = base_path(sprintf('routes/%s.php', $filename));
        if (!file_exists($route)) {
            throw new RuntimeException('This specified route file does not exist.');
        }

        return $route;
    }

    /**
     * Writing route.
     *
     * @param string $routePath
     * @param string $route
     */
    public function writeRoute($routePath, $route)
    {
        file_put_contents($routePath, $route, FILE_APPEND);
    }

    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getNameInput(): string
    {
        $name = trim($this->argument('name'));
        $name = strtolower($name);

        return $name;
    }

    /**
     * Build replacement values.
     *
     * @param  string  $name
     * @return array
     */
    protected function buildReplacements(string $name): array
    {
        return [
            'DummyModalName' => Str::studly(class_basename($name)),
            'DummyVariableName' => lcfirst(class_basename($name))
        ];
    }

    /**
     * Replace values for the given stub.
     *
     * @param  string  $stub
     * @param  array  $replace
     * @return string
     */
    protected function replaceReplacements(string $stub, array $replace): string
    {
        $stub = str_replace(
            array_keys($replace),
            array_values($replace),
            $stub
        );

        return $stub;
    }
}
