<?php

namespace Ihero\Generator\Console\Commands\General;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;

class JsGeneratorCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ih.make:js
                                {name : The name of js file.}
                                {--scaffold= : Scaffold\'s js.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new js file.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Js';

    /**
     * Get the stub file for the generator.
     *
     * @param  string  $type
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/../../../stubs/resources.js.stub';
    }

    /**
     * Execute the console command.
     *
     * @return bool|null
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        if ($this->option('scaffold')) {
            $this->buildFileByScaffold($this->option('scaffold'));
        } else {
            $this->buildFile($this->getNameInput());
        }

        $this->info($this->type.' created successfully.');
    }

    /**
     * Build view file by scaffold.
     *
     * @param string $type
     * @return void
     */
    public function buildFileByScaffold(string $type)
    {
        if ($stub = config(sprintf('ih.stubs.%s.resources.js', $type))) {
            $name = sprintf('%s/index', $this->getNameInput());

            $this->buildFile($name, $stub);
        }
    }

    /**
     * Build view file.
     *
     * @param string $name
     * @param string $stub
     * @return void
     */
    public function buildFile($name, $stub = '')
    {
        $path = $this->getPath($name);

        $this->makeDirectory($path);

        $this->files->put($path, $this->buildView($name, $stub ?: $this->getStub()));
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        return $this->laravel->resourcePath().'/js/'.str_replace('\\', '/', $name).'.js';
    }

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @param  string  $stub
     * @return string
     */
    protected function buildView($name, $stub)
    {
        $stub = $this->files->get($stub);
        $replace = $this->buildReplacements($name);

        return $this
            ->replaceReplacements($stub, $replace);
    }

    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getNameInput()
    {
        $name = trim($this->argument('name'));
        $name = strtolower($name);

        return $name;
    }

    /**
     * Build the model replacement values.
     *
     * @param  string  $name
     * @return array
     */
    protected function buildReplacements($name)
    {
        if (count($array = explode('/', $name)) > 1) {
            $array = array_slice($array, -2, 1);
            $name = array_pop($array);
        }

        return [
            'DummyClassName' => Str::studly(class_basename($name)),
            'DummyVariableName' => lcfirst(class_basename($name))
        ];
    }

    /**
     * Replace the model class for the given stub.
     *
     * @param  string  $stub
     * @param  array  $replace
     * @return $this
     */
    protected function replaceReplacements(string $stub, array $replace)
    {
        $stub = str_replace(
            array_keys($replace),
            array_values($replace),
            $stub
        );

        return $stub;
    }
}
