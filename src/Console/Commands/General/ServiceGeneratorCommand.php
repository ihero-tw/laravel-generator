<?php

namespace Ihero\Generator\Console\Commands\General;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;
use Ihero\Generator\Exceptions\RuntimeException;

class ServiceGeneratorCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ih.make:service
                                {name : The name of the class.}
                                {--repo= : The name of repository.}
                                {--empty : Blank version of service.}
                                {--scaffold= : Scaffold\'s service.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new service class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Service';

    /**
     * Get the stub file for the generator.
     *
     * @param  string  $type
     * @return string
     */
    protected function getStub(string $type = '')
    {
        if ($type == 'repo') {
            return __DIR__.'/../../../stubs/service.repo.stub';
        } elseif ($stub = config(sprintf('ih.stubs.%s.service', $type))) {
            return $stub;
        }
        return __DIR__.'/../../../stubs/service.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Services';
    }

     /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        if ($this->option('empty')) {
            $stub = $this->files->get($this->getStub());

            return $this
                ->replaceNamespace($stub, $name)
                ->replaceClass($stub, $name);
        } elseif ($this->option('scaffold')) {
            $stub = $this->files->get($this->getStub($this->option('scaffold')));
            $repoClass = $this->parseReop($this->getNameInput());
            $replace = $this->buildRepoReplacements($repoClass);

            return $this
                ->replaceNamespace($stub, $name)
                ->replaceRepoClass($stub, $replace)
                ->replaceClass($stub, $name);
        } else {
            $stub = $this->files->get($this->getStub('repo'));
            $repoClass = $this->parseReop($this->option('repo') ?? $this->getNameInput());
            $replace = $this->buildRepoReplacements($repoClass);

            return $this
                ->alongWith($repoClass)
                ->replaceNamespace($stub, $name)
                ->replaceRepoClass($stub, $replace)
                ->replaceClass($stub, $name);
        }
    }

    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getNameInput()
    {
        $name = trim($this->argument('name'));
        $name = Str::studly($name);

        return Str::is('*Service', $name) ?
            $name : Str::finish($name, 'Service');
    }

    /**
     * Get the fully-qualified model class name.
     *
     * @param  string  $model
     * @return string
     *
     * @throws RuntimeException
     */
    protected function parseReop($repository)
    {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $repository)) {
            throw new RuntimeException('Repository name contains invalid characters.');
        }

        $repository = trim(str_replace('/', '\\', $repository), '\\');
        $repository = str_replace('Service', '', $repository);

        if (! Str::is('*Repository', $repository)) {
            $repository = Str::finish($repository, 'Repository');
            $repository = Str::afterLast($repository, '\\');
        }
        if (Str::startsWith($repository, $rootNamespace = $this->laravel->getNamespace())) {
            $repository = class_basename($repository);
        }

        return sprintf('%sRepositories\\%s', $rootNamespace, $repository);
    }

    /**
     * Build the model replacement values.
     *
     * @return array
     */
    protected function buildRepoReplacements($repoClass)
    {
        return [
            'DummyFullRepositoryClass' => $repoClass,
            'DummyRepositoryClass' => class_basename($repoClass),
            'DummyRepositoryVariable' => lcfirst(class_basename($repoClass)),
            'DummyModelName' => Str::studly(str_replace('Repository', '', class_basename($repoClass))),
            'DummyVariableName' => strtolower(str_replace('Repository', '', class_basename($repoClass)))
        ];
    }

    /**
     * Build along with repository class.
     *
     * @param string $repoClass
     * @return $this
     */
    protected function alongWith($repoClass)
    {
        if (! class_exists($repoClass)) {
            if ($this->confirm("A {$repoClass} repository does not exist. Do you want to generate it?", true)) {
                $this->call('ih.make:repository', ['name' => $repoClass]);
            }
        }

        return $this;
    }

    /**
     * Replace the model class for the given stub.
     *
     * @param  string  $stub
     * @param  array  $replace
     * @return $this
     */
    protected function replaceRepoClass(string &$stub, array $replace)
    {
        $stub = str_replace(
            array_keys($replace),
            array_values($replace),
            $stub
        );

        return $this;
    }
}
