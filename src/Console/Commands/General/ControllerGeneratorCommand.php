<?php

namespace Ihero\Generator\Console\Commands\General;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;
use Ihero\Generator\Exceptions\RuntimeException;

class ControllerGeneratorCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ih.make:controller
                                {name : The name of the class.}
                                {--blade : By default, all of the controller methods use the Blade view.}
                                {--serv= : The name of service.}
                                {--empty : Blank version of controller.}
                                {--scaffold= : Scaffold\'s controller.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new controller class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Controller';

    /**
     * Get the stub file for the generator.
     *
     * @param  string  $type
     * @return string
     */
    protected function getStub(string $type = '')
    {
        if ($type == 'serv') {
            return $this->option('blade') ?
                __DIR__.'/../../../stubs/controller.serv.blade.stub':
                __DIR__.'/../../../stubs/controller.serv.inertia.stub';
        } elseif ($stub = config(sprintf('ih.stubs.%s.controller', $type))) {
            return $stub;
        }
        return __DIR__.'/../../../stubs/controller.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Http\Controllers';
    }

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        if ($this->option('empty')) {
            $stub = $this->files->get($this->getStub());

            return $this
                ->replaceNamespace($stub, $name)
                ->replaceClass($stub, $name);
        } elseif ($this->option('scaffold')) {
            $stub = $this->files->get($this->getStub($this->option('scaffold')));
            $servClass = $this->parseServ($this->getNameInput());
            $replace = $this->buildServReplacements($servClass);

            return $this
                ->replaceNamespace($stub, $name)
                ->replaceServClass($stub, $replace)
                ->replaceClass($stub, $name);
        } else {
            $stub = $this->files->get($this->getStub('serv'));
            $servClass = $this->parseServ($this->option('serv') ?? $this->getNameInput());
            $replace = $this->buildServReplacements($servClass);

            return $this
                ->alongWith($servClass)
                ->replaceNamespace($stub, $name)
                ->replaceServClass($stub, $replace)
                ->replaceClass($stub, $name);
        }
    }

    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getNameInput()
    {
        $name = trim($this->argument('name'));
        $name = Str::studly($name);

        return Str::is('*Controller', $name) ?
            $name : Str::finish($name, 'Controller');
    }

    /**
     * Get the fully-qualified model class name.
     *
     * @param  string  $model
     * @return string
     *
     * @throws RuntimeException
     */
    protected function parseServ($service)
    {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $service)) {
            throw new RuntimeException('Service name contains invalid characters.');
        }

        $service = trim(str_replace('/', '\\', $service), '\\');
        $service = str_replace('Controller', '', $service);

        if (! Str::is('*Service', $service)) {
            $service = Str::finish($service, 'Service');
            $service = Str::afterLast($service, '\\');
        }
        if (Str::startsWith($service, $rootNamespace = $this->laravel->getNamespace())) {
            $service = class_basename($service);
        }

        return sprintf('%sServices\\%s', $rootNamespace, $service);
    }

    /**
     * Build the model replacement values.
     *
     * @param  string  $servClass
     * @return array
     */
    protected function buildServReplacements($servClass)
    {
        return [
            'DummyFullServiceClass' => $servClass,
            'DummyServiceClass' => class_basename($servClass),
            'DummyServiceVariable' => lcfirst(class_basename($servClass)),
            'DummyModelName' => Str::studly(str_replace('Service', '', class_basename($servClass))),
            'DummyVariableName' => str_replace('Service', '', class_basename($servClass))
        ];
    }

    /**
     * Build along with service class.
     *
     * @param string $servClass
     * @return $this
     */
    protected function alongWith($servClass)
    {
        if (! class_exists($servClass)) {
            if ($this->confirm("A {$servClass} service does not exist. Do you want to generate it?", true)) {
                $this->call('ih.make:service', ['name' => $servClass]);
            }
        }

        return $this;
    }

    /**
     * Replace the model class for the given stub.
     *
     * @param  string  $stub
     * @param  array  $replace
     * @return $this
     */
    protected function replaceServClass(string &$stub, array $replace)
    {
        $stub = str_replace(
            array_keys($replace),
            array_values($replace),
            $stub
        );

        return $this;
    }
}
