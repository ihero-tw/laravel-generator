<?php
namespace Ihero\Generator;

use Illuminate\Support\ServiceProvider;

class GeneratorServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     *
     * @return void
     */
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/ih.php', 'ih');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands('Ihero\Generator\Console\Commands\General\ControllerGeneratorCommand');
        $this->commands('Ihero\Generator\Console\Commands\General\ServiceGeneratorCommand');
        $this->commands('Ihero\Generator\Console\Commands\General\RepositoryGeneratorCommand');
        $this->commands('Ihero\Generator\Console\Commands\General\CriteriaGeneratorCommand');
        $this->commands('Ihero\Generator\Console\Commands\General\ModelGeneratorCommand');
        $this->commands('Ihero\Generator\Console\Commands\General\ViewGeneratorCommand');
        $this->commands('Ihero\Generator\Console\Commands\General\JsGeneratorCommand');
        $this->commands('Ihero\Generator\Console\Commands\General\RouteGeneratorCommand');

        $this->commands('Ihero\Generator\Console\Commands\Helper\HashPasswordGeneratorCommand');

        $this->mergeConfigs();
        $this->loadHelpers();
    }

    /**
     *
     * @return void
     */
    public function mergeConfigs()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/ih.php', 'ih');
    }

    /**
     *
     * @return void
     */
    public function loadHelpers()
    {
        foreach (glob(__DIR__ . '/Helpers/*.php') as $file) {
            require_once($file);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
