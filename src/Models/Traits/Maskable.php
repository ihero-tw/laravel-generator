<?php

namespace Ihero\Generator\Models\Traits;

use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Model;

trait Maskable
{
    protected $rules = [
        'name' => '*,1,-1',
        'phone' => '*,4,-3',
        'address' => '*,3',
        'mail' => '*,1,-3|before:@',
        'bank_account' => '*,4,-4',
        'identity' => '*,3,-3',
        'certificate' => '*,4,-4',
        'company_certificate' => '*,2,-2',
    ];

    /**
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function setAttribute($key, $value)
    {
        // 防止誤將遮罩字串寫入 DB
        // 只要包含遮罩字串就取回原始資料，並覆蓋它
        if (Arr::exists($this->masked, $key)) {
            if (filled($rule = $this->getRule($this->masked[$key])) && Str::contains($value, $rule['character'])) {
                $value = $this->getAttribute($key, true);
            }
        }

        parent::setAttribute($key, $value);
    }

    /**
     *
     * @param string $key
     * @return string
     */
    public function getAttribute($key, $unmask = false)
    {
        $relationValue = parent::getAttribute($key);

        if ($relationValue instanceof Model && method_exists($relationValue, 'dataMask')) {
            $relationValue->dataMask();
        } elseif (!$unmask && filled($relationValue)) {
            if (Arr::exists($this->masked, $key)) {
                $relationValue = $this->mask($this->masked[$key], $relationValue);
            } elseif (is_array($relationValue)) {
                $relationValue = $this->jsonMask($key, $relationValue);
            }
        }

        return $relationValue;
    }

     /**
     *
     * @return void
     */
    public function dataMask()
    {
        $this->attributes = collect($this->attributes)->map(function ($attribute, $key) {
            return (Arr::exists($this->masked, $key) && filled($attribute)) ?
                $this->mask($this->masked[$key], $attribute): $attribute;
        })->toArray();

        $this->original = collect($this->original)->map(function ($attribute, $key) {
            return (Arr::exists($this->masked, $key) && filled($attribute)) ?
                $this->mask($this->masked[$key], $attribute): $attribute;
        })->toArray();
    }

    /**
     *
     * @param string $rule
     * @param string $value
     * @return string
     */
    protected function mask(String $rule, String $value)
    {
        if (filled($rule = $this->getRule($rule))) {
            if ($rule['method'] == 'before') {
                $afterStr = Str::after($value, $rule['search']);
                $beforeStr = Str::before($value, $rule['search']);
                $beforeStr = $this->toMask($rule, $beforeStr);

                return sprintf('%s%s%s', $beforeStr, $rule['search'], $afterStr);
            }

            return $this->toMask($rule, $value);
        }
        return $value;
    }

    /**
     *
     * @param array $rule
     * @param string $value
     * @return string
     */
    protected function toMask(Array $rule, String $value)
    {
        if ($rule['length'] < 0 && mb_strlen($value) <= ($rule['index'] + abs($rule['length']))) {
            $rule['length'] = null;
        }

        return Str::mask($value, $rule['character'], $rule['index'], $rule['length'] ?? null);
    }

    /**
     *
     * @param string $ruleName
     * @return mixed
     */
    protected function getRule(String $ruleName)
    {
        $method = 'fill';
        $character = $index = $length = $method = $search = '';
        if (isset($this->rules[$ruleName]) && filled($rule = $this->rules[$ruleName])) {
            if (Str::contains($rule, '|')) {
                list($rule, $method) = explode('|', $rule);
                list($method, $search) = explode(':', $method);
            }

            $parts = explode(',', $rule);
            list($character, $index, $length) = array_pad($parts, 3, null);

            return compact('character', 'index', 'length', 'method', 'search');

        /** 功能先寫死，未來再抽象化模組 */
        } elseif (Str::contains($ruleName, 'mask_if')) {
            $ruleStrings = explode('|', $ruleName);
            $rule = collect($ruleStrings)->transform(function ($ruleString) {
                $ruleString = Str::afterLast($ruleString, 'mask_if:');
                list($anotherField, $fieldValue, $ruleName) = explode(',', $ruleString);

                return compact('anotherField', 'fieldValue', 'ruleName');
            })->first(function ($rule) {
                return $this->{$rule['anotherField']} == $rule['fieldValue'];
            });

            return filled($rule)? $this->getRule($rule['ruleName']): null;
        }
        return ;
    }

    /**
     *
     * @param string $key
     * @param array $value
     * @return array
     */
    protected function jsonMask(String $modelKey, Array $value)
    {
        if (filled($rule = $this->getJsonRule($modelKey))) {
            $rule->map(function ($rule, $key) use (&$value) {
                $path = Str::after($key, '->');
                $path = Str::replace('->', '.', $path);

                if (filled($data = data_get($value, $path))) {
                    data_set($value, $path, $this->mask($rule, $data));
                }
            });
        }

        return $value;
    }

    /**
     *
     * @param string $modelKey
     * @return collection
     */
    protected function getJsonRule(String $modelKey)
    {
        $rule = collect($this->masked);
        $rule = $rule->filter(function ($value, $key) use ($modelKey) {
            return Str::contains($key, '->') && Str::contains($key, $modelKey);
        });

        return $rule;
    }

    /**
     *
     * @param string $key
     * @return mixed
     */
    public function unmask(String $key)
    {
        return $this->getAttribute($key, true);
    }
}
