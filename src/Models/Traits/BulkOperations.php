<?php

namespace Ihero\Generator\Models\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

trait BulkOperations
{
    public static function getTableName()
    {
        return DB::getTablePrefix() . with(new self)->getTable();
    }

    /**
     *
     * @param Array $rows
     * @return void
     */
    public static function batchNewOrUpdate(Array $rows)
    {
        $table = self::getTableName();
        $columns = self::getInsertColumns($rows);
        $values = self::getInsertValues($rows);
        $updates = self::getInsertUptates($rows);

        $sql = "INSERT INTO {$table}({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";
        return DB::statement($sql);
    }

    /**
     *
     * @param array $rows
     * @return void
     */
    protected static function getInsertColumns(array $rows)
    {
        $first = reset($rows);
        $first['created_at'] = '';
        return implode(
            ',',
            array_map(function ($value) {
                return "$value";
            }, array_keys($first))
        );
    }

    /**
     *
     * @param array $rows
     * @return void
     */
    protected static function getInsertValues(array $rows)
    {
        $datetimeNow = Carbon::now();
        return implode(
            ',',
            array_map(function ($row) use ($datetimeNow) {
                return '(' . implode(
                    ',',
                    array_map(function ($value) {
                        return '"' . str_replace('"', '""', $value) . '"';
                    }, $row)
                ) . ', "' . $datetimeNow . '")';
            }, $rows)
        );
    }

    /**
     *
     * @param array $rows
     * @return void
     */
    protected static function getInsertUptates(array $rows)
    {
        $first = reset($rows);
        $datetimeNow = Carbon::now();
        $first['updated_at'] = '';
        return implode(
            ',',
            array_map(function ($value) use ($datetimeNow) {
                if ($value == 'updated_at') {
                    return 'updated_at = "'.$datetimeNow.'"';
                } else {
                    return "$value = VALUES($value)";
                }
            }, array_keys($first))
        );
    }

    /**
     *
     * @param Array $rows
     * @return void
     */
    public static function batchCreate(Array $rows)
    {
        $table = self::getTableName();
        $columns = self::getInsertColumns($rows);
        $values = self::getInsertValues($rows);

        $sql = "INSERT IGNORE INTO {$table}({$columns}) VALUES {$values}";
        return DB::statement($sql);
    }

    /**
     *
     * @param Array $multipleData
     * @return void
     */
    public function batchUpdate(Array $multipleData)
    {
        $multipleData = $this->formatModelData($multipleData);
        $tableName = self::getTableName();
        $firstRow = current($multipleData);
        $updateColumn = array_keys($firstRow);

        // 預設以id為條件更新，如果沒有ID則以第一個欄位為條件
        $referenceColumn = isset($firstRow['id']) ? 'id' : current($updateColumn);
        unset($updateColumn[0]);

        // 拼接sql語句
        $updateSql = "UPDATE " . $tableName . " SET ";
        $sets  = [];
        $bindings = [];
        foreach ($updateColumn as $uColumn) {
            $setSql = "`" . $uColumn . "` = CASE ";
            foreach ($multipleData as $data) {
                $setSql .= "WHEN `" . $referenceColumn . "` = ? THEN ? ";
                $bindings[] = $data[$referenceColumn];
                $bindings[] = $data[$uColumn];
            }
            $setSql .= "ELSE `" . $uColumn . "` END ";
            $sets[] = $setSql;
        }

        $updateSql .= implode(', ', $sets);
        $whereIn = collect($multipleData)->pluck($referenceColumn)->values()->all();
        $bindings = array_merge($bindings, $whereIn);
        $whereIn = rtrim(str_repeat('?,', count($whereIn)), ',');
        $updateSql = rtrim($updateSql, ", ") . " WHERE `" . $referenceColumn . "` IN (" . $whereIn . ")";

        // 傳入預處理sql語句和對應繫結資料
        return DB::update($updateSql, $bindings);
    }

    /**
     * 格式化欄位為物件型態的資料
     *
     * @param Array $data
     * @return Array
     */
    protected function formatModelData(Array $data)
    {
        return collect($data)->map(function ($value) {
            $formatData = array();
            foreach ($value as $key => $val) {
                $formatData[$key] = $val;
                if (array_key_exists($key, $this->casts)) {
                    if ($this->{$key} = json_decode($val, true)) {
                        $formatData[$key] = json_encode($this->{$key});
                    }
                }
            }
            return $formatData;
        })->all();
    }
}
