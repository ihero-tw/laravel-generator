<?php
namespace Ihero\Generator\Models\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;

trait DuplicateRelations
{
    public function duplicate(Array $relations = [])
    {
        $new = $this->replicate();
        $new->push();

        $this->relations = [];
        $this->load(...$relations);

        self::duplicateRelations($this, $new);

        return $new;
    }

    public static function duplicateRelations($from, $to)
    {
        foreach ($from->relations as $relationName => $object) {
            if ($object !== null) {
                if ($object instanceof Collection) {
                    foreach ($object as $relation) {
                        self::replication($relationName, $relation, $to);
                    }
                } else {
                    self::replication($relationName, $object, $to);
                }
            }
        }
    }

    private static function replication($name, $relation, $to)
    {
        if (self::learnMethodType($to, $name) == 'BelongsToMany') {
            $to->{$name}()->sync($relation);
        } else {
            $newRelation = $relation->replicate();
            $new = $to->{$name}()->create($newRelation->toArray());

            if ($relation->relations !== null) {
                self::duplicateRelations($relation, $new);
            }
        }
    }

    private static function learnMethodType($classname, $method)
    {
        $oReflectionClass = new \ReflectionClass($classname);
        $method = $oReflectionClass->getMethod($method);
        $type = get_class($method->invoke($classname));
        return Str::afterLast($type, '\\');
    }
}
