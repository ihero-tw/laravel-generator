<?php

namespace Ihero\Generator\Models\Traits;

trait ProxyModel
{
    /**
     * Get an attribute from the prodcut model.
     *
     * @param string $attribute
     * @return mixed
     */
    public function __get($key)
    {
        if (in_array($key, $this->proxyAttributes)) {
            return $this->getAttribute($key);
        }
        return $this->getAttribute($this->proxyModel)->{$key};
    }
}
