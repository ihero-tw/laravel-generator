<?php

namespace Ihero\Generator\Repositories\Criteria;

use Ihero\Generator\Repositories\Contracts\RepositoryInterface;

abstract class Criteria
{
    /**
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    abstract public function apply($model, RepositoryInterface $repository);
}
