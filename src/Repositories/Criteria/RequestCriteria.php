<?php
namespace Ihero\Generator\Repositories\Criteria;

use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Ihero\Generator\Repositories\Criteria\Criteria;
use Ihero\Generator\Repositories\Contracts\RepositoryInterface;

/**
 * Class RequestCriteria
 * @package Ihero Generator
 * @author AJie <aj@ihero.tw>
 */
class RequestCriteria extends Criteria
{
    /**
     * @var string $search
     */
    protected $search;

    /**
     * @var string $ask
     */
    protected $ask;

    public function __construct(
        protected Request $request
    ) {
        $this->search = $request->search ?? null;
        $this->ask = $request->ask ?? null;
    }

    /**
     * Apply criteria in query repository
     *
     * @param Builder|Model $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (filled($this->search)) {
            $searchable = ($model instanceof Builder) ?
                $model->getModel()->searchable : $model->searchable;

            $blueprint = $this->blueprint($searchable, $this->search, $this->ask);
            $model = $this->builder($model, $blueprint);
        }

        return $model;
    }

    /**
     *
     * @param array $searchable
     * @param string $search
     * @param mixed $ask
     * @return array
     */
    protected function blueprint(Array $searchable, String $search, ?String $ask)
    {
        $askCondition = $this->parserAsk($ask);
        $condition = $this->filledCondition($searchable, $askCondition);

        $fields = $this->parserRule($search);
        $fields = $this->splitFieldAndValue($fields);

        // 快速搜尋不走這裡
        $fields = $this->checkLegalAndAddCondition($condition, $fields);

        $fields = $this->splitRelationModelString($fields);
        $fields = $this->filledLikeQuerySign($fields);
        $fields = $this->filledTimeIntervalMarker($fields);

        $this->request->session()->push('_flash.old', '_old_input');

        return $fields;
    }

    protected function parserAsk(?String $ask)
    {
        $fields = [];

        if (filled($ask)) {
            $this->request->session()->flash('_old_input.ask', true);

            if (stripos($ask, ';')) {
                $bucket = explode(';', $ask);
            } else {
                $bucket[] = $ask;
            }

            foreach ($bucket as $field) {
                if (stripos($field, ':')) {
                    list($table['name'], $table['condition']) = explode(':', $field);
                    $fields[$table['name']] = $table['condition'];
                }
            }
        }

        return $fields;
    }

    protected function filledCondition(Array $fields, Array $askCondition)
    {
        foreach ($fields as $field => $condition) {
            if (is_numeric($field)) {
                Arr::forget($fields, $field);
                $fields = Arr::add($fields, $condition, '=');
            }
        }

        $fields = array_merge($fields, $askCondition);

        return $fields;
    }

    /**
     * Parser AND OR Rule.
     *
     * 陣列組成即是規則本身，陣列第一層是 AND 組合, 第二層為 OR 的組合
     *
     * @param string $search
     * @return array
     */
    protected function parserRule(String $search)
    {
        $fields = [];

        // Split AND
        if (stripos($search, ';')) {
            $fields = explode(';', $search);
        } else {
            $fields[] = $search;
        }

        // Split OR
        foreach ($fields as $key => $field) {
            if (stripos($field, ',')) {
                $fields[$key] = explode(',', $field);
            }
        }

        return $fields;
    }

    /**
     * Summary of splitFieldAndValue
     * @param array $fields
     * @return array
     */
    protected function splitFieldAndValue(Array $fields)
    {
        foreach ($fields as $parentKey => $value) {
            if (is_array($value)) {
                foreach ($value as $subKey => $val) {
                    $fields[$parentKey][$subKey] = $this->splitNameAndValue($val);
                }
            } else {
                $fields[$parentKey] = $this->splitNameAndValue($value);
            }
        }

        return $fields;
    }

    protected function splitNameAndValue(String $field)
    {
        if (stripos($field, ':')) {
            $name = Str::before($field, ':');
            $value = Str::after($field, ':');

            $this->request->session()->flash('_old_input.'.$name, $value);

            if (stripos($value, '|')) {
                $value = explode('|', $value);
            }

            return [
                'name' => $name,
                'value' => $value
            ];
        } else {
            // 找不到任何分號字串時，回傳欄位本身字串表示採用快速搜尋
            return $field;
        }
    }

    protected function checkLegalAndAddCondition(Array $modelSearchable, Array $fields)
    {
        foreach ($fields as $parentKey => $field) {
            if (!array_key_exists('name', $field)) {
                foreach ($field as $subKey => $subField) {
                    if (array_key_exists($subField['name'], $modelSearchable)) {
                        $fields[$parentKey][$subKey]['condition'] = $modelSearchable[$subField['name']];
                    } else {
                        Arr::forget($fields, $parentKey.'.'.$subKey);
                    }
                }
            } elseif (!isset($modelSearchable[$field['name']])) {
                Arr::forget($fields, $parentKey);
            } else {
                $fields[$parentKey]['condition'] = $modelSearchable[$field['name']];
            }
        }
        $fields = array_filter($fields);

        return $fields;
    }

    protected function splitRelationModelString(Array $fields)
    {
        foreach ($fields as $parentKey => $field) {
            if (!isset($field['name'])) {
                foreach ($field as $subKey => $subField) {
                    list(
                        $fields[$parentKey][$subKey]['name'],
                        $fields[$parentKey][$subKey]['relation']
                    ) = $this->spliting($subField['name']);
                }
            } else {
                list(
                    $fields[$parentKey]['name'],
                    $fields[$parentKey]['relation']
                ) = $this->spliting($field['name']);
            }
        }

        return $fields;
    }

    protected function spliting(String $field)
    {
        $splitUp = explode('.', $field);
        $field = array_pop($splitUp);
        $relation = implode('.', $splitUp);

        return [
            $field,
            $relation
        ];
    }

    protected function filledLikeQuerySign(Array $fields)
    {
        foreach ($fields as $parentKey => $field) {
            if (!array_key_exists('name', $field)) {
                foreach ($field as $subKey => $subField) {
                    if ($subField['condition'] == 'like') {
                        $fields[$parentKey][$subKey]['value'] = "%{$subField['value']}%";
                    }
                }
            } else {
                if ($field['condition'] == 'like') {
                    $fields[$parentKey]['value'] = "%{$field['value']}%";
                }
            }
        }

        return $fields;
    }

    protected function filledTimeIntervalMarker(array $fields)
    {
        foreach ($fields as $parentKey => $field) {
            if (!array_key_exists('name', $field)) {
                foreach ($field as $subKey => $subField) {
                    if ($subField['condition'] == 'between' && count($subField['value']) > 1) {
                        $fields[$parentKey][$subKey]['value'][0] = $this->isDateTimeFormat($subField['value'][0]) ?
                            $subField['value'][0].' 00:00:00' : $subField['value'][0];
                        $fields[$parentKey][$subKey]['value'][1] = $this->isDateTimeFormat($subField['value'][1]) ?
                            $subField['value'][1].' 23:59:59' : $subField['value'][1];
                    }
                }
            } else {
                if ($field['condition'] == 'between' && count($field['value']) > 1) {
                    $fields[$parentKey]['value'][0] = $this->isDateTimeFormat($field['value'][0]) ?
                        $field['value'][0].' 00:00:00' : $field['value'][0];
                    $fields[$parentKey]['value'][1] = $this->isDateTimeFormat($field['value'][1]) ?
                        $field['value'][1].' 23:59:59' : $field['value'][1];
                }
            }
        }

        return $fields;
    }

    protected function isDateTimeFormat(string $string)
    {
        return preg_match('/^\d{4}-\d{2}-\d{2}$/', $string) === 1;
    }

    protected function builder($model, Array $blueprint)
    {
        foreach ($blueprint as $field) {
            if (array_key_exists('name', $field)) {
                $model = $this->buildSingleCondition($model, $field);
            } else {
                $model = $this->buildManyCondition($model, $field);
            }
        }

        return $model;
    }

    protected function buildSingleCondition($query, Array $field)
    {
        if ($field['relation'] && Str::startsWith($field['relation'], 'morph.')) {
            $relation = Str::after($field['relation'], 'morph.');

            return $query->whereHasMorph($relation, '*', function ($query) use ($field) {
                $query = $this->getCondition($query, $field);
            });
        } elseif ($field['relation']) {
            return $query->whereHas($field['relation'], function ($query) use ($field) {
                $query = $this->getCondition($query, $field);
            });
        } else {
            return $this->getCondition($query, $field);
        }
    }

    protected function getCondition($query, Array $field)
    {
        if ($field['condition'] == 'between') {
            return $query->whereBetween($field['name'], $field['value']);
        } elseif ($field['condition'] == 'notbetween') {
            return $query->whereNotBetween($field['name'], $field['value']);
        } elseif (is_array($field['value'])) {
            return $query->whereIn($field['name'], $field['value']);
        } else {
            return $query->where($field['name'], $field['condition'], $field['value']);
        }
    }

    protected function buildManyCondition($query, Array $fields)
    {
        return $query->where(function ($query) use ($fields) {
            $query = $this->buildSingleCondition($query, array_shift($fields));

            foreach ($fields as $field) {
                if ($field['relation'] && Str::startsWith($field['relation'], 'morph.')) {
                    $relation = Str::after($field['relation'], 'morph.');

                    return $query->whereHasMorph($relation, '*', function ($query) use ($field) {
                        $query->where($field['name'], $field['condition'], $field['value']);
                    });
                } elseif ($field['relation']) {
                    $query->orWhereHas($field['relation'], function ($query) use ($field) {
                        $query->where($field['name'], $field['condition'], $field['value']);
                    });
                } else {
                    $query->orWhere($field['name'], $field['condition'], $field['value']);
                }
            }
            return $query;
        });
    }
}
