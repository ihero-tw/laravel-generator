<?php

namespace Ihero\Generator\Repositories;

use Ihero\Generator\Exceptions\RepositoryException;
use Ihero\Generator\Repositories\Contracts\CriteriaInterface;
use Ihero\Generator\Repositories\Contracts\RepositoryInterface;
use Ihero\Generator\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use ReflectionClass;

abstract class Repository implements RepositoryInterface, CriteriaInterface
{
    /**
     * This repositories the model.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * Reserve model infomation for reset model use.
     *
     * @var \ReflectionClass
     */
    protected $reflectionModel;

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $criteria;

    /**
     * @var bool
     */
    protected $skipCriteria = false;


    /**
     * Find model record all
     *
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all(array $columns = ['*'])
    {
        $this->applyCriteria();

        $model = $this->model->get($columns);

        $this->skipCriteria(false);
        $this->resetCriteria();
        $this->resetModel();

        return $model;
    }

    /**
     * Find model record all by LazyCollection
     *
     * @param array $columns
     * @return \Illuminate\Support\LazyCollection
     */
    public function cursor(array $columns = ['*'])
    {
        $this->applyCriteria();

        $model = $this->model->cursor($columns);

        $this->skipCriteria(false);
        $this->resetCriteria();
        $this->resetModel();

        return $model;
    }

    /**
     * Find model record and limit return
     *
     * @param int $limit
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function limit(int $limit, array $columns = ['*'])
    {
        $this->applyCriteria();

        $model = $this->model
            ->limit($limit)
            ->get($columns);

        $this->skipCriteria(false);
        $this->resetCriteria();
        $this->resetModel();

        return $model;
    }

    /**
     * Find model record for given id
     *
     * @param mixed $id
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection|Illuminate\Database\Eloquent\Model|array|null
     */
    public function find(mixed $id, array $columns = ['*'])
    {
        $this->applyCriteria();

        $model = $this->model->find($id, $columns);

        $this->skipCriteria(false);
        $this->resetCriteria();
        $this->resetModel();

        return $model;
    }

    /**
     * Find model record for given id
     *
     * @param array $columns
     *
     * @return Illuminate\Database\Eloquent\Model|null
     */
    public function first(array $columns = ['*'])
    {
        $this->applyCriteria();

        $model = $this->model->first($columns);

        $this->skipCriteria(false);
        $this->resetCriteria();
        $this->resetModel();

        return $model;
    }

    /**
     * Get the model by id.
     *
     * @param string|int $id
     * @return \Illuminate\Database\Eloquent\Collection|Illuminate\Database\Eloquent\Model|null
     */
    public function getTrashedById(string|int $id)
    {
        return $this->model->onlyTrashed()->find($id);
    }

    /**
     * Make a model.
     *
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function make(array $data = [])
    {
        $model = $this->model->newInstance();
        $model->fill($data);

        return $model;
    }

    /**
     * Create a row record.
     *
     * @param array $data
     * @param array $options
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data, array $options = [])
    {
        $model = $this->make($data);
        $model->save($options);

        return $model;
    }

    /**
     * Update the row record by primary key id.
     *
     * @param string|int $id
     * @param array $data
     * @param  array $options
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update(string|int $id, array $data, array $options = [])
    {
        $model = $this->model->findOrFail($id);
        $model->fill($data);
        $model->save($options);

        return $model;
    }

    /**
     * Update the data row by model.
     *
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @param  array $data
     * @param  array $options
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function updateByModel($model, array $data, array $options = [])
    {
        $model->update($data, $options);

        return $model;
    }

    /**
     * Delete the row record by primary key id.
     *
     * @param array|Collection|int|string $ids
     * @return int
     */
    public function delete(array|Collection|int|string $ids): int
    {
        return $this->model->destroy($ids);
    }

    /**
     * Delete the row by model.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return bool
     */
    public function deleteByModel($model): bool
    {
        return $model->delete();
    }

    /**
     * When need paginate fileted data, can use this method.
     *
     * @param int $perPage
     * @param array $columns
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate(int $perPage = 15, array $columns = ['*'])
    {
        $this->applyCriteria();

        $model = $this->model->paginate($perPage, $columns);

        $this->skipCriteria(false);
        $this->resetCriteria();
        $this->resetModel();

        return $model;
    }

    /**
     * Get database query builder.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function getQuery()
    {
        $this->applyCriteria();

        $builder = $this->model->getQuery();

        $this->skipCriteria(false);
        $this->resetCriteria();
        $this->resetModel();

        return $builder;
    }

    /**
     * Get database query builder.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function toSql()
    {
        $this->applyCriteria();

        dd($this->model->toSql());
    }


    /**
     * Union query.
     *
     * @param \Illuminate\Database\Query\Builder $builder
     * @return $this
     */
    public function union($builder)
    {
        $this->model = $this->model->union($builder);

        return $this;
    }

    /**
     * Sort the row record.
     *
     * @param string $column
     * @return $this
     */
    public function sortBy(string $column, string $sort = 'asc')
    {
        $this->applyCriteria();
        $this->skipCriteria();

        $this->model = $this->model->orderby($column, $sort);

        return $this;
    }

    /**
     * Sort the row record.
     *
     * @param string $column
     * @param string $sort
     * @return $this
     */
    public function sortByDesc(string $column)
    {
        $this->applyCriteria();
        $this->skipCriteria();

        $this->model = $this->model->orderby($column, 'desc');

        return $this;
    }

    /**
     * @return $this
     */
    public function resetCriteria()
    {
        $this->criteria = collect([]);

        return $this;
    }

    /**
     * @param bool $status
     * @return $this
     */
    public function skipCriteria($status = true)
    {
        $this->skipCriteria = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCriteria()
    {
        return $this->criteria ?? collect([]);
    }

    /**
     * @param Criteria $criteria
     *
     * @return $this
     */
    public function getByCriteria(Criteria $criteria)
    {
        $this->model = $criteria->apply($this->model, $this);

        return $this;
    }

    /**
     * @param Criteria $criteria
     *
     * @return $this
     */
    public function pushCriteria(Criteria $criteria)
    {
        if (! optional($this->criteria)->push($criteria)) {
            $this->criteria = (new Collection)->push($criteria);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function applyCriteria()
    {
        if ($this->skipCriteria === true) {
            return $this;
        }

        foreach ($this->getCriteria() as $criteria) {
            if ($criteria instanceof Criteria) {
                $this->model = $criteria->apply($this->model, $this);
            }
        }

        return $this;
    }

    /**
     * @throws RepositoryException
     */
    protected function resetModel()
    {
        $model = App::make(
            $this->reflectionModel->name
        );

        if (!$model instanceof Model) {
            throw new RepositoryException(
                "Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model"
            );
        }

        return $this->model = $model;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return $model
     */
    protected function reserve($model)
    {
        $this->reflectionModel = new \ReflectionClass($model);

        return $model;
    }

    /**
     * Call the Repository magic method.
     *
     * @param string $method
     * @param array $args
     * @return void
     */
    public function __call(string $method, array $args)
    {
        if (count($args) == 1) {
            if (Str::is('getBy*', $method)) {
                return $this->getBy(substr($method, 5), $args[0]);
            } elseif (Str::is('firstBy*', $method)) {
                return $this->firstBy(substr($method, 7), $args[0]);
            }
        }
    }

    /**
     * Find model record for given column and return first record.
     *
     * @param string $column
     * @param string|int $value
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getBy(string $column, string|int $value)
    {
        if (filled($value)) {
            $column = $this->molded($column);
            $model = $this->modelWhere($column, $value);

            $this->resetCriteria();
            $this->resetModel();

            return $model->get();
        }
        return collect([]);
    }

    /**
     * Find model record for given column.
     *
     * @param string $column
     * @param string|int $value
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function firstBy(string $column, string|int $value)
    {
        if (filled($value)) {
            $column = $this->molded($column);
            $model = $this->modelWhere($column, $value);

            $this->resetCriteria();
            $this->resetModel();

            return $model->first();
        }
        return null;
    }

    /**
     * Adjust variate string to snake_case.
     *
     * @param string $column
     * @return string
     */
    protected function molded(String $column): string
    {
        $column = Str::studly($column);
        $column = Str::snake($column);

        return $column;
    }

    /**
     * Get the model data by condition.
     *
     * @param string $column
     * @param string|int $arg
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function modelWhere(string $column, string|int $value)
    {
        $this->applyCriteria();

        return $this->model->where($column, $value);
    }
}
