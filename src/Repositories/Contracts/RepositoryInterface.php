<?php

namespace Ihero\Generator\Repositories\Contracts;

use Illuminate\Support\Collection;

interface RepositoryInterface
{

    public function all(array $columns = ['*']);

    public function paginate(int $perPage = 15, array $columns = ['*']);

    public function create(array $data);

    public function update(string|int $id, array $data);

    public function delete(array|Collection|int|string $ids): int;

    public function find(int|array $id, array $columns = ['*']);
}
