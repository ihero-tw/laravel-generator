## Laravel Generantor

IHero Laravel Generantor Features

- Generantor Commands
    - Controller
    - Repository
    - Criteria
    - Service
    - Model
    - Views

### 安裝方法

#### 在新 Laravel 專案中的執行以下指令

** 加入安裝來源
```
composer config repositories.ihero-generator vcs https://ihero-ajieeeee@bitbucket.org/ihero-tw/laravel-generator.git
```

** 安裝套件
```
composer require ihero/generator
```

** 加入指令 (Laravel 5 以上不需執行)

在 Laravel 專案 config/app.php 中，將 `Ihero\Generantor\Providers\IheroGeneratorServiceProvider::class` 加到 `providers` 陣列變數中

```
'providers' => [
    ...
    Ihero\Generantor\Providers\IheroGeneratorServiceProvider::class,
],
```


### 指令

```
php artisan ih.make:controller {$NAME} --serv={$SERVICE_NAME}     Controller generator command
php artisan ih.make:service {$NAME} --repo={$REPOSITORY_NAME}     Service generator command
php artisan ih.make:repository {$NAME} --model={$Model_NAME}      Repository generator command
php artisan ih.make:views {$NAME}                                 View generator command
php artisan ih.make:criteria {$NAME}                              Criteria generator command

php artisan ih.scaffold:cms                                       Create CMS scaffold. Include views, routes, config, translations of Adminlte.
php artisan ih.scaffold:listing                                   Create data listing scaffold.
php artisan ih.scaffold:nestable
```

### CMS 搜尋功能實作

#### 後台端

在 Service 宣告物件 RequestCriteria
```
    use Ihero\Generator\Repositories\Criteria\RequestCriteria;
```

在 method 利用 Repository 的 `pushCriteria()` 將 RequestCriteria 注入，並帶入 Request Collection
```
    public function find(Request $request)
    {
        return $this->adminsRepository
            ->pushCriteria(new RequestCriteria($request))
            ->paginate();
    }
```

在 Model 加入 $searchable
```
    public $searchable = [
        'name',
        'product.sku' => 'like',
        'product.admins.id',
        'start_date_at' => '<=',
        'end_date_at' => '>=',
    ];
```
> 請注意使用 public 宣告此變數


#### 前台端

注入 js
```
    search-form.js
```

form 表單設定

預設搜尋是使用 or 想要用 and 請加入 searchJoin=and
```
    <input name="searchJoin" value="and">
```

有時在介面上只使用一個輸入欄位，並一次想搜尋資料庫二個以上欄位
```
    <input name="product.sku,product.name" value="iphone">
```

使用上面的搜尋設定時，通常都是使用 or 的查詢。有時複雜的介面會在伴隨其他 and 的欄位條件查詢，可使用 `searchJoinExclude` 排除 and 欄位
```
    <input name="searchJoinExclude" value="product.sku,product.name">
```

#### 路由解析

路由參數
```
    http://ihero.dev/users?search={field}:{value}&searchJoin=and&searchJoinExclude={field},{field}

    searchFields=
    filter=
    orderBy=
    sortedBy=
    with=
    withCount=

```
> 參考：https://github.com/andersao/l5-repository#using-the-requestcriteria

一般搜尋:
```
    ?search=name:aj
```

快速搜尋
```
    ?search=aj
```

多欄位 or :
```
    ?search=name:aj,age:18
```

多欄位 and :
```
    ?search=name:aj;age:18
```

有 or 及 and 混合的搜尋:
```
    ?search=name:aj,nickname:aj;age:18
```

高級用法：
```
    找出 姓名或綽號是 AJ 且 年齡是 18 歲 且 姓別是男或女性 且 身高大於 180

    ?search=name:aj,nickname:aj;age:18;gender:male|female,height:180&ask=height:gt

    找出 ...

    ?search=no:00958404240001;product.name:iphone,product.sku:iphone;store.no:0988,store.name:0988,store.manager.phone:0988;campaign.event_start_date_at:2020-04-01|2020-05-01&ask=campaign.event_start_date_at:between
```

使用多態關聯：
```
    在搜尋字串及 Model 的 $searchable 內的多態欄位名稱加入 `morph` 的前缀字

    例：
        ?search=morph.name:aj

    $searchable = [
        'morph.name' => 'like'
    ]
```

查詢 Json 欄位：
```
    在搜尋字串及 Model 的 $searchable 內的欄位名稱加入 `->` 箭頭符號

    例：
        ?search=onwer->name:aj

    $searchable = [
        'onwer->name' => 'like'
    ]
```


### Criteria 與 Repository 運用情境

如果所使用的條件需 return $this 給下一個 method 使用時請將放置於 Criteria

Repository 只執行 return、delete data 等等操作.

### 常用套件

#### 權限

```
composer require spatie/laravel-permission
```

#### 語系轉換

```
composer require spatie/laravel-translatable
```

#### Log 記錄

```
composer require spatie/laravel-activitylog
```

#### AdminLTE for laravel

(Doc)[https://github.com/jeroennoten/Laravel-AdminLTE#610-menu]

```
composer require jeroennoten/laravel-adminlte
```

#### Bootstrap File Input

```
https://plugins.krajee.com/file-input
```

#### Bootstrap Select

```
https://developer.snapappointments.com/bootstrap-select/
```

#### Date Range Picker

```
https://www.daterangepicker.com/
```

#### Laravel Debugbar
```
https://github.com/barryvdh/laravel-debugbar
```